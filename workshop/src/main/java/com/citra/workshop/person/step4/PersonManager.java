package com.citra.workshop.person.step4;

import java.util.ArrayList;
import java.util.List;

//Scalable
public class PersonManager {

  public static void main(String[] args) {
	
	List<Person> people = new ArrayList<>();
	
	Person alan = new Person("Alan", "Turing", Gender.Male);
	people.add(alan);
	
	Person joan = new Person("Joan", "Clarke", Gender.Female);
	people.add(joan);
	
	Person james = new Person("James", "Gosling", Gender.Male);
	people.add(james);

	for(Person p : people) {
	  System.out.println(p.getFullName());
	}
	
  }
  

}
