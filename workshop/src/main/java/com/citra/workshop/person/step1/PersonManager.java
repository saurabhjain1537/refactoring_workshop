package com.citra.workshop.person.step1;

import java.util.ArrayList;
import java.util.List;

public class PersonManager {

  public static void main(String[] args) {
	
	List<Person> people = new ArrayList<>();
	
	Person p1 = new Person("Alan", "Turing", "Male");
	people.add(p1);
	
	Person p2 = new Person("Joan", "Clarke", "Female");
	people.add(p2);
	
	Person p3 = new Person("James", "Gosling", "Male");
	people.add(p3);
	
	for(Person p : people) {
	  System.out.print(getValue(p.getGender()));
	  System.out.println(" " + p.getFirstName() + " " + p.getLastName());
	}
	
  }
  
  private static String getValue(String input) {
	
	if(input.equalsIgnoreCase("Male")) {
	  return "Mr";
	} else {
	  return "Ms";
	}
		
  }

}
