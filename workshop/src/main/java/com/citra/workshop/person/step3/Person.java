package com.citra.workshop.person.step3;

public class Person {


  private final String firstName;
  private final String lastName;
  private final Gender gender;
  
  public Person(String firstName, String lastName, Gender gender) {
	
	if(firstName == null || lastName == null || gender == null) {
	  throw new IllegalArgumentException("Mandatory Paramater missing.");
	  
	}
	
	this.firstName = firstName;
	this.lastName = lastName;
	this.gender = gender;
  }
  
  public String getFirstName() {
    return firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public Gender getGender() {
    return gender;
  }
  
}
