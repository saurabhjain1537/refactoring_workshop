package com.citra.workshop.person.step2;

import java.util.ArrayList;
import java.util.List;

//Maintainable
public class PersonManager {

  public static void main(String[] args) {
	
	List<Person> people = new ArrayList<>();
	
	Person alan = new Person("Alan", "Turing", "Male");
	people.add(alan);
	
	Person joan = new Person("Joan", "Clarke", "Female");
	people.add(joan);
	
	Person james = new Person("James", "Gosling", "Male");
	people.add(james);
	
	for(Person p : people) {
	  System.out.println(getFullName(p));
	}
	
  }
  
  private static String getFullName(Person p) {
	return getSalutation(p.getGender()) + " " + p.getFirstName() + " " + p.getLastName();
  }
  
  private static String getSalutation(String gender) {
	
	String salutation = null;
	if(gender.equalsIgnoreCase("Male")) {
	  salutation = "Mr";
	} else {
	  salutation = "Ms";
	}
	
	return salutation;
	
  }

}
