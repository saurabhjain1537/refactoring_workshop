package com.citra.workshop.person.step3;

public class PersonValidationException extends RuntimeException{

  public PersonValidationException(String arg0, Throwable arg1) {
	super(arg0, arg1);
  }

  public PersonValidationException(String arg0) {
	super(arg0);
  }

}
