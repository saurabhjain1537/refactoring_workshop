package com.citra.workshop.person.step3;

import java.util.ArrayList;
import java.util.List;

//Robust
public class PersonManager {

  public static void main(String[] args) {
	
	List<Person> people = new ArrayList<>();
	
	Person alan = new Person("Alan", "Turing", Gender.Male);
	people.add(alan);
	
	Person joan = new Person("Joan", "Clarke", Gender.Female);
	people.add(joan);
	
	Person james = new Person("James", "Gosling", Gender.Male);
	people.add(james);

	for(Person p : people) {
	  System.out.println(getFullName(p));
	}
	
  }
  
  private static String getFullName(Person p) {
	return getSalutation(p.getGender()) + " " + p.getFirstName() + " " + p.getLastName();
  }
  
  private static String getSalutation(Gender gender) {
	
	String salutation = null;
	if(Gender.Male == gender) {
	  salutation = "Mr";
	} else if(Gender.Female == gender) {
	  salutation = "Ms";
	} else {
	  throw new PersonValidationException("Unsupported Gender " + gender);
	}
	
	return salutation;
	
  }

}
