package com.citra.workshop.person.step4;

public class Person {


  private final String firstName;
  private final String lastName;
  private final Gender gender;
  
  public Person(String firstName, String lastName, Gender gender) {
	
	if(firstName == null || lastName == null || gender == null) {
	  throw new IllegalArgumentException("Mandatory Paramater missing.");
	  
	}
	
	this.firstName = firstName;
	this.lastName = lastName;
	this.gender = gender;
  }
  
  public String getFirstName() {
    return firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public Gender getGender() {
    return gender;
  }
  
  public String getSalutation() {
	
	String salutation = null;
	if(Gender.Male == gender) {
	  salutation = "Mr";
	} else if(Gender.Female == gender) {
	  salutation = "Ms";
	} else {
	  throw new PersonValidationException("Unsupported Gender " + gender);
	}
	
	return salutation;
	
  }
  
  public String getFullName( ) {
	
	return getSalutation() + " " + getFirstName() + " " + getLastName();
  }
  
}
